using System.Threading;
using Android.App;
using Android.OS;

namespace ASSN3.Droid
{
    [Activity(Label = "Assessment 3", Icon = "@drawable/Ass3Logo", Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashScreenActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Thread.Sleep(1000);
            StartActivity(typeof(MainActivity));
        }
    }
}