﻿using System;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class AddURLPage : ContentPage
	{
		private SQLiteAsyncConnection _connection;

		public AddURLPage()
		{
			InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

		async void AddURL(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TitleNew.Text))
            {
                await DisplayAlert("Validation Error", "Title cannot be empty.", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(ImageNew.Text) && string.IsNullOrWhiteSpace(URLNew.Text))
            {
                await DisplayAlert("Validation Error", "Both image text and url text cannot be empty.", "OK");
                return;
            }

            var url = new WebURL
		    {
               Title = TitleNew.Text,
               Image = ImageNew.Text,
               Url = URLNew.Text
		    };

		    await _connection.InsertAsync(url);
		    await DisplayAlert("Add New URL", "URL Added Successfully", "OK");

		    TitleNew.Text = string.Empty;
		    ImageNew.Text = string.Empty;
		    URLNew.Text = string.Empty;
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}

	}
}
