﻿using System;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class EditURLPage : ContentPage
	{
	    private SQLiteAsyncConnection _connection;
	    private ObservableCollection<WebURL> _url;
	    private int _selectedNumber;

		public EditURLPage(WebURL SelectedClass, int Selection)
		{
			InitializeComponent();

		    _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
		    BindingContext = SelectedClass;
		    _selectedNumber = Selection;
		}

		async void EditURL(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TitleNew.Text))
            {
                await DisplayAlert("Validation Error", "Title cannot be empty.", "OK");
                return;
            }

            if (string.IsNullOrWhiteSpace(ImageNew.Text) && string.IsNullOrWhiteSpace(URLNew.Text))
            {
                await DisplayAlert("Validation Error", "Both image text and url text cannot be empty.", "OK");
                return;
            }

            try
		    {
		        var url = await _connection.Table<WebURL>().ToListAsync();
		        _url = new ObservableCollection<WebURL>(url);

		        if (_selectedNumber >= 0 && _url != null)
		        {
		            var myURL = _url[_selectedNumber];
		            myURL.Title = TitleNew.Text;
		            myURL.Url = URLNew.Text;
		            myURL.Image = ImageNew.Text;

		            await _connection.UpdateAsync(myURL);
		            await DisplayAlert("Success", "Successfully Updated the URL", "OK");
		        }
		    }
		    catch (Exception Exception)
		    {
		        await DisplayAlert("Exception Details", "An Exception Has Been Thrown, Please Try Again. \r\n If This Persists Please Contact the Helpdesk", "OK");
		    }

		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}