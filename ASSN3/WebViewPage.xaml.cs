﻿using System;

using Xamarin.Forms;

namespace ASSN3
{
	public partial class WebViewShow : ContentPage
	{
		public WebViewShow(string url)
		{
			InitializeComponent();

		    if (!string.IsNullOrEmpty(url))
		    {
		        ShowWeb.Source = url;
		    }
        }

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}
