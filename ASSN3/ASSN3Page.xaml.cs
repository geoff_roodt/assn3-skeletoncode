﻿using System.Collections.Generic;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{
        [PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		public string Url { get; set; }
		public string Image { get; set; }
		public string Title { get; set; }
	}

	public partial class ASSN3Page : ContentPage
	{
	    private SQLiteAsyncConnection _connection;
	    private List<WebURL> _urls;
	    private bool _isNavigating;

		public ASSN3Page()
		{
			InitializeComponent();
		    _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
		    _isNavigating = false;
		}

		async void ReloadPicker()
		{
		    await _connection.CreateTableAsync<WebURL>();
            _urls = await _connection.Table<WebURL>().ToListAsync();

			if (ShowURlS.Items.Count != 0)
			{
				ShowURlS.Items.Clear();
			}

			foreach (var x in _urls)
			{
				ShowURlS.Items.Add($"{x.Title}");
			}
		}

		protected override void OnAppearing()
		{
			ReloadPicker();
		    _isNavigating = false;
			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
		    if (!_isNavigating)
		    {
		        _isNavigating = true;
		        if (_urls != null && ShowURlS?.SelectedIndex >= 0)
		        {
		            var url = _urls[ShowURlS.SelectedIndex].Url;
		            var image = _urls[ShowURlS.SelectedIndex].Image;

		            await Navigation.PushModalAsync(new WebViewShow(url??image));
		        }
		        else
		        {
		            await DisplayAlert("Error", "Please select a url from the list", "OK");
		            _isNavigating = false;
		        }
		    }
		}

		async void AddURLAction(object sender, System.EventArgs e)
		{
		    if (!_isNavigating)
		    {
		        _isNavigating = true;
		        await Navigation.PushModalAsync(new AddURLPage());
		    }
		}

		async void UpdateURLAction(object sender, System.EventArgs e)
        {
            if (!_isNavigating)
            {
                _isNavigating = true;
                if (_urls != null && ShowURlS?.SelectedIndex >= 0)
                {
                    _urls = await _connection.Table<WebURL>().ToListAsync();
                    var selectedItems = ShowURlS.SelectedIndex;
                    var selectedClass = _urls[ShowURlS.SelectedIndex];

                    await Navigation.PushModalAsync(new EditURLPage(selectedClass, selectedItems));
                }
                else
                {
                    await DisplayAlert("Error", "Please select a url to update", "OK");
                    _isNavigating = false;
                }
            }
        }

		async void RemoveURLAction(object sender, System.EventArgs e)
		{
		    if (!_isNavigating)
		    {
		        _isNavigating = true;
		        if (_urls != null && ShowURlS?.SelectedIndex >= 0)
		        {
		            _urls = await _connection.Table<WebURL>().ToListAsync();
		            var selectedItems = _urls[ShowURlS.SelectedIndex];

		            await _connection.DeleteAsync(selectedItems);
		            await DisplayAlert("Deleted URL", $"{selectedItems.Title} deleted successfully", "OK");
		            ReloadPicker();
		        }
		        else
		        {
		            await DisplayAlert("Error", "Please select a url to delete", "OK");
		        }
		        _isNavigating = false;
		    }
		}
	}
}